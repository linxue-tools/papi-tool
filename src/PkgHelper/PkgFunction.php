<?php

declare(strict_types=1);

/**
 * 辅助库: 函数
 */

if (!function_exists('router_autoloader')) {

    /**
     * 自动加载路由
     * @desc 路由文件名后缀统一格式为 xxx.route.php
     * @param string $path
     */
    function router_autoloader(string $path)
    {
        $subRouteFiles = glob(rtrim($path, '/') . '/*.route.php');
        foreach ($subRouteFiles as $subRouteFile) {
            include_once $subRouteFile;
        }
    }
}

#