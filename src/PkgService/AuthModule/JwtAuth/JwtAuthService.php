<?php

declare(strict_types=1);

namespace Linxue\PapiTool\PkgService\AuthModule\JwtAuth;

use Hyperf\Utils\Arr;
use Lcobucci\JWT\Encoding\ChainedFormatter;
use Lcobucci\JWT\Encoding\JoseEncoder;
use Lcobucci\JWT\Signer\Hmac\Sha256;
use Lcobucci\JWT\Signer\Key\InMemory;
use Lcobucci\JWT\Token\Builder;

/**
 * 应用服务: Jwt校验
 * @date    2023-01-11 09:42:33
 */
class JwtAuthService
{

    /**
     * 多维数组按键key排序
     * @param array $data
     * @return array
     */
    public function deepArrKsort(array $data = []): array
    {
        foreach ($data as $k => $item) {
            $data[$k] = is_array($item) ? $this->deepArrKsort($item) : $item;
        }
        ksort($data);
        return $data;
    }

    /**
     * 将请求的参数加密并生成签名
     *
     * @param array $requestParams
     * @param array $signConfig
     * @param bool $debug
     * @return string|array
     */
    public function createJwtSignature(
        array $requestParams = [],
        array $signConfig = []
    ): string {
        $ts = isset($requestParams['ts']) ? $requestParams['ts'] : 0;
        $formatParams = Arr::except($requestParams, ['key', 'sign', 'ts', 'debug']);
        $formatParams = (array)$formatParams;
        ## 1.参数排序
        $sortParams = $this->deepArrKsort($formatParams);
        ## 2.参数编码
        $encodeParams = http_build_query($sortParams);
        ## 3.参数的加密
        $md5Params = md5($encodeParams);
        ## 配置
        $signPrefix = isset($signConfig['sign_prefix']) ? trim($signConfig['sign_prefix']) : '';
        $signKey = isset($signConfig['sign_key']) ? trim($signConfig['sign_key']) : '';
        $signSecret = isset($signConfig['sign_secret']) ? trim($signConfig['sign_secret']) : '';
        $sign = md5($signPrefix . $signKey . $signSecret . $ts . $md5Params);
        ## 生成的新签名
        return (string)$sign;
    }

    /**
     * 生成jwt的token
     * @return string
     */
    public function createJwtToken(
        int $staffId = 0,
        string $appKey = ''
    ): string {
        $res = '';
        $appHost = '';
        $tokenKey = '';
        $publicKey = '';
        $appPrivateKey = '';

        ## getTokenKey 类型必须严格
        $tokenKey = $this->getTokenKey();

        // $appHost = $this->jwtAuthConfigService()->getAppHost();
        // $tokenKey = $this->jwtAuthConfigService()->getTokenKey();
        // var_dump(__METHOD__ . __LINE__);
        // var_dump($tokenKey);
        // $publicKey = $this->jwtAuthConfigService()->getAppKey();
        // $appPrivateKey = $this->jwtAuthConfigService()->getPrivateKey();
        // $expiredTime = $this->jwtAuthBasicService()->createExpiredTime();
        try {
            $tokenBuilder = (new Builder(new JoseEncoder(), ChainedFormatter::default()));
            $algorithm    = new Sha256();
            $token = $tokenBuilder
                ->issuedBy($appHost)
                ->permittedFor($appHost)
                // ->identifiedBy('4f1g23a12aa')
                ## 自定义参数
                ->withClaim('created', time()) // 生成时间
                ->withClaim('staff_id', $staffId)  // 用户id
                ->withClaim('public_key', $publicKey)  // 公钥
                ->withClaim('secrect', $appPrivateKey) // 私钥
                ->withClaim('app_host', $appHost) // 主机
                ->getToken($algorithm, $tokenKey);

            $res = $token->toString();
        } catch (\Exception $e) {
            ## 生成token出现异常了
            $errMsg = $e->getMessage();
        }
        return (string)$res;
    }

    // /**
    //  * 解析jwt的token
    //  * @param string $token token
    //  * @return array token解析后的数据
    //  */
    // public function parseToken(string $oriToken = ''): array
    // {
    //     $res = [];
    //     $jwtData = [];
    //     $errMsg = '';
    //     $debug = [];
    //     try {
    //         $parser = new Parser(new JoseEncoder());
    //         $jwtParseRes = $parser->parse($oriToken);
    //         $errMsg = PkgConstant::TIP_MSG_OK;
    //         ## 实际数据
    //         $jwtData['created'] = $jwtParseRes->claims()->get('created');
    //         $jwtData['staff_id'] = $jwtParseRes->claims()->get('staff_id');
    //         $jwtData['public_key'] = $jwtParseRes->claims()->get('public_key');
    //         $jwtData['secrect'] = $jwtParseRes->claims()->get('secrect');
    //         $jwtData['app_host'] = $jwtParseRes->claims()->get('app_host');

    //         ## 调试
    //         $debug['iss'] = $jwtParseRes->claims()->get('iss');
    //         $debug['aud'] = $jwtParseRes->claims()->get('aud');
    //         $debug['jti'] = $jwtParseRes->claims()->get('jti');
    //         $debug['exp'] = $jwtParseRes->claims()->get('exp');
    //         $debug['iat'] = $jwtParseRes->claims()->get('iat');
    //     } catch (CannotDecodeContent | InvalidTokenStructure | UnsupportedHeaderFound $e) {
    //         $errMsg = $e->getMessage();
    //     }
    //     ## 返回
    //     $res = [
    //         'ori_token' => $oriToken,
    //         'jwt_data' => $jwtData,
    //         'err_msg' => $errMsg,
    //     ];
    //     ## 调试模式
    //     if ($this->jwtAuthConfigService()->getDebugStatus()) {
    //         $res['debug'] = $debug;
    //     }

    //     return (array)$res;
    // }


    /**
     * 生成过期的时间戳
     */
    public function createExpiredTime()
    {
        // $expiredSecond = $this->jwtAuthConfigService()->getExpiredSecord();
        $expiredSecond = 3200;
        $expiredTime = time() + $expiredSecond;
        return $expiredTime;
    }

    /**
     * jwt必备配置: 获取key
     */
    public function getTokenKey()
    {
        $tokenKey = 'xxxxxxxx';
        $tokenKey = trim($tokenKey);
        $key = InMemory::base64Encoded($tokenKey);
        return $key;
    }

    #
}
