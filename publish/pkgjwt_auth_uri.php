<?php


/**
 * config/autoload/pkgjwt_auth_uri.php
 * 配置: jwt请求uri授权名单
 * @desc 补充说明: '白名单'与'授权名单'的区别在于 '白名单'无需校验sign. 
 * @desc '白名单'适用于例如支付回调或第三方回调
 * @date 2023-01-16 11:46:04
 */
return [
    'default' => [
        '/index/default',
        '/index/getToken',
        '/index/checkJwt',
    ],
];
